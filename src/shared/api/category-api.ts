import { useQuery } from 'react-query';

export type Category = {
  name: string;
  count: number;
  date_in: Date;
};

export const useCategories = () =>
  useQuery<Category[]>('/categories', {
    retry: false,
  });
