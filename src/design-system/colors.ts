export const Colors = {
  Primary: '#BDBDC7',
  Secondary: '#505050',
  Tertiary: '#FDFDFD',
  Red: '#FF0101',
  White: '#ffffff',
  Black: '#000000',
};
