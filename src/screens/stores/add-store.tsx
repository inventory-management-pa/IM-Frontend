import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Colors } from '../../design-system';
import { Button, Input } from '../../design-system/components';
import { NavigationRoutes, RootStackParamList } from '../../navigation';

type AddStoreProps = NativeStackScreenProps<
  RootStackParamList,
  NavigationRoutes.StoreAddition
>;

export const AddStore: React.FC<AddStoreProps> = ({ navigation }) => {
  return (
    <View style={styles.addStoreContainer}>
      <Input label="Store Name" style={styles.addStoreInput} required />
      <Input label="Manager Name" style={styles.addStoreInput} required />
      <Input label="Type of Stock" style={styles.addStoreInput} required />
      <Button
        text="Add Store"
        style={styles.addStoreBtn}
        onPress={() => navigation.navigate(NavigationRoutes.Products)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  addStoreContainer: {
    flex: 1,
    backgroundColor: Colors.Tertiary,
    alignItems: 'center',
    paddingTop: 63,
  },
  addStoreInput: {
    marginBottom: 10,
    width: '85%',
  },
  addStoreBtn: {
    position: 'absolute',
    bottom: 97,
  },
});
