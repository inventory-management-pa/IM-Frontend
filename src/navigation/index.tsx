import { createNativeStackNavigator } from '@react-navigation/native-stack';

export type RootStackParamList = {
  [NavigationRoutes.Login]: undefined;
  [NavigationRoutes.SignUp]: undefined;
  [NavigationRoutes.Stores]: undefined;
  [NavigationRoutes.StoreAddition]: undefined;
  [NavigationRoutes.Categories]: undefined;
  [NavigationRoutes.CategoryAddition]: undefined;
  [NavigationRoutes.Products]: undefined;
  [NavigationRoutes.ProductAddition]: undefined;
};

export const Stack = createNativeStackNavigator<RootStackParamList>();

export enum NavigationRoutes {
  Login = 'Login',
  SignUp = 'SignUp',
  Stores = 'Stores',
  StoreAddition = 'StoreAddition',
  Categories = 'Categories',
  CategoryAddition = 'CategoryAddition',
  Products = 'Products',
  ProductAddition = 'ProductAddition',
}
