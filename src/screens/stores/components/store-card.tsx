import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Colors } from '../../../design-system';

type StoreCardProps = {
  onPress: () => void;
};

export const StoreCard: React.FC<StoreCardProps> = ({ onPress }) => {
  return (
    <View
      style={styles.storeCard}
      onStartShouldSetResponder={() => true}
      onResponderStart={onPress}
    >
      <Text style={styles.storeCardTitle}>Luffy Store</Text>
      <View style={styles.storeCardTable}>
        <View style={styles.storeCardCol}>
          <Text style={styles.storeCardColText}>Manager</Text>
          <Text style={styles.storeCardVal}>Naruto</Text>
        </View>
        <View style={styles.storeCardCol}>
          <Text style={styles.storeCardColText}>Type</Text>
          <Text style={styles.storeCardVal}>Electronics</Text>
        </View>
        <View style={styles.storeCardCol}>
          <Text style={styles.storeCardColText}>Category Count</Text>
          <Text style={styles.storeCardVal}>8</Text>
        </View>
        <View style={styles.storeCardCol}>
          <Text style={styles.storeCardColText}>Net worth</Text>
          <Text style={styles.storeCardVal}>3 crores</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  storeCard: {
    width: '90%',
    height: 275,
    alignItems: 'center',
    backgroundColor: Colors.Secondary,
    borderRadius: 40,
    marginBottom: 20,
    // Shadow.
    shadowColor: Colors.Black,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
  },
  storeCardTitle: {
    marginTop: 20,
    fontSize: 16,
  },
  storeCardTable: {
    width: '100%',
    height: '80%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    // borderWidth: 1,
    marginTop: 10,
  },
  storeCardCol: {
    height: '40%',
    width: '40%',
    alignItems: 'center',
    // borderWidth: 1,
    margin: 9,
  },
  storeCardColText: {
    fontSize: 14,
    marginBottom: 10,
    fontWeight: 'bold',
  },
  storeCardVal: {
    fontSize: 16,
  },
});
