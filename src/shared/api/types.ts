export type ApiResponse<T = any> = {
  message?: string;
  payload: T;
};

export type ApiErrorResponse<T = any> = {
  type: ApiErrorType;
  message: string;
};

export type ApiError = {
  status_code: number;
  type: ApiErrorType;
  message?: string;
  details?: string[];
};

export enum ApiErrorType {
  Authorization = 'authorization',
  ValidationError = 'ValidationError',
}
