import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import { Colors } from '../../design-system';
import { AddButton } from '../../design-system/components';
import { NavigationRoutes, RootStackParamList } from '../../navigation';
import { StoreCard } from './components/store-card';
type StoresProps = NativeStackScreenProps<
  RootStackParamList,
  NavigationRoutes.Stores
>;
export const Stores: React.FC<StoresProps> = ({ navigation }) => {
  const onPress = () => {
    navigation.navigate(NavigationRoutes.StoreAddition);
  };

  const onStoreCardPress = () => {
    navigation.navigate(NavigationRoutes.Categories);
  };

  // Dummy data.
  const storeData = [
    {
      id: '1',
    },
    {
      id: '2',
    },
    {
      id: '3',
    },
    {
      id: '4',
    },
    {
      id: '5',
    },
  ];
  const renderItem = ({ item }) => <StoreCard onPress={onStoreCardPress} />;

  return (
    <View style={styles.storesContainer}>
      <FlatList
        data={storeData}
        style={{ backgroundColor: Colors.Tertiary }}
        contentContainerStyle={styles.storesListContainer}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
      <AddButton onPress={onPress} />
    </View>
  );
};

const styles = StyleSheet.create({
  storesContainer: {
    flex: 1,
  },
  storesListContainer: {
    flex: 1,
    alignItems: 'center',
    padding: 21,
  },
});
